import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import search from './ducks/search/reducer';

const store = createStore(search, composeWithDevTools(applyMiddleware(thunk)));

export default store;
