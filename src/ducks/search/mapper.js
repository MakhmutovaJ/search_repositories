import { get } from 'lodash';

const transformData = item => ({
  url: get(item, 'html_url', ''),
  name: get(item, 'name', ''),
  stars: get(item, 'stargazers_count', ''),
  watchers: get(item, 'watchers_count', ''),
});

const searchMapper = (data) => {
  const newData = data.data.items.map(item => transformData(item));
  return newData;
};

export default searchMapper;
