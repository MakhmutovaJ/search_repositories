import { createReducer, createAction } from 'redux-act';
import axios from 'axios';
import searchMapper from './mapper';

const initialState = { projects: [] };

export const searchSuccess = createAction('SEARCH_SUCCESS');
export const searchFailure = createAction('SEARCH_FAILURE');

export const asyncSearch = searchQuery => (dispatch) => {
  const response = axios.get(
    `https://api.github.com/search/repositories?q=${searchQuery}`,
  );
  response.then((data) => {
    const mappedSearch = searchMapper(data);
    dispatch(searchSuccess(mappedSearch));
  }, (error) => {
    dispatch(searchFailure(error));
  });
};

const handleSearchSuccess = (state, data) => ({
  projects: data,
});


const reducer = createReducer((on) => {
  on(searchSuccess, handleSearchSuccess);
}, initialState);

export default reducer;
