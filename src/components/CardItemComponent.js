import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { makeStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';

const useStyles = makeStyles(() => ({
  card: {
    borderRadius: 15,
    margin: '20px',
  },
  item: {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'wrap',
    width: 250,
  },
  typography: {
    paddingTop: 10,
    margin: 10,
    width: 100,
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
  },
  link: {
    textDecoration: 'none',
    color: 'black',
  },
  line: {
    display: 'flex',
  },
  field: {
    paddingTop: 10,
    margin: 10,
  },
}));


const CardItemComponent = ({ data }) => {
  const {
    url, name, stars, watchers,
  } = data;

  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <CardContent className={classes.item}>
        <div className={classes.line}>
          <Typography color="primary" className={classes.field}> Name: </Typography>
          <Typography color="secondary" className={classes.typography}>
            {/* eslint-disable-next-line */}
            <a href={url} target="_blank" className={classes.link}>
              {name}
            </a>
          </Typography>
        </div>
        <div className={classes.line}>
          <Typography color="primary" className={classes.field}> Stars: </Typography>
          <Typography color="secondary" className={classes.typography}>
            {stars}
          </Typography>
        </div>
        <div className={classes.line}>
          <Typography color="primary" className={classes.field}> Watchers: </Typography>
          <Typography color="secondary" className={classes.typography}>
            {watchers}
          </Typography>
        </div>
      </CardContent>
    </Card>
  );
};

CardItemComponent.propTypes = {
  data: PropTypes.shape({
    url: PropTypes.string,
    name: PropTypes.string,
    stars: PropTypes.number,
    watchers: PropTypes.number,
  }).isRequired,
};

export default CardItemComponent;
