import React from 'react';
import { makeStyles } from '@material-ui/core';
import PropTypes from 'prop-types';
import CardItemComponent from './CardItemComponent';

const useStyles = makeStyles(() => ({
  card: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
    margin: '0 auto',
    maxWidth: 900,
  },
}));

const ProjectListComponent = ({ projects }) => {
  const classes = useStyles();

  return (
    <div className={classes.card}>
      {projects.map((item, index) => (
        <CardItemComponent key={`${index}${item.name}`} data={item} />
      ))}
    </div>
  );
};

ProjectListComponent.propTypes = { projects: PropTypes.arrayOf(PropTypes.object).isRequired };

export default ProjectListComponent;
