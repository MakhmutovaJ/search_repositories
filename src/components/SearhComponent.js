import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import Clear from '@material-ui/icons/Clear';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';

const useStyles = makeStyles(() => ({
  filter: {
    display: 'flex',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    flexDirection: 'center',
    maxWidth: 900,
    margin: '0 auto',
  },
  search: {
    display: 'flex',
    flexGrow: 3,
    margin: '30px 10px',
  },
  error: {
    paddingLeft: 20,
  },
  searchButton: {
    margin: '30px 10px',
    flexGrow: 0.5,
  },
}));

const SearchComponent = ({
  onChange, onClear, onClick, searchText,
}) => {
  const classes = useStyles();

  return (
    <div className={classes.filter}>
      <FormControl margin="none" className={classes.search}>
        <TextField
          autoFocus
          value={searchText}
          variant="outlined"
          placeholder="Searching Movie"
          onChange={onChange}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton onClick={onClear}>
                  <Clear />
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
      </FormControl>
      <Button
        color="primary"
        variant="contained"
        className={classes.searchButton}
        onClick={onClick}
      >
                Search
      </Button>
    </div>
  );
};

SearchComponent.propTypes = { searchText: PropTypes.string.isRequired };

export default SearchComponent;
