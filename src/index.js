import React, { Fragment } from 'react';
import { render } from 'react-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import { Provider } from 'react-redux';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';
import grey from '@material-ui/core/colors/grey';
import App from './containers/App';
import store from './store';

const appElement = document.getElementById('root');

const theme = createMuiTheme({ palette: { primary: blue, secondary: grey } });

render(
  <Provider store={store}>
    <MuiThemeProvider theme={theme}>
      <Fragment>
        <CssBaseline />
        <App />
      </Fragment>
    </MuiThemeProvider>
  </Provider>,
  appElement,
);
