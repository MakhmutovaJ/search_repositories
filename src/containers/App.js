import React, { Component } from 'react';
import { connect } from 'react-redux';
import { throttle } from 'throttle-debounce';
import PropTypes from 'prop-types';
import SearchComponent from '../components/SearhComponent';
import { asyncSearch } from '../ducks/search/reducer';
import ProjectListComponent from '../components/ProjectListComponent';

const initialState = {
  searchText: '',
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
    this.searchThrottled = throttle(300, this.search);
  }

    search = () => {
      const { searchText } = this.state;
      const { searchRequest } = this.props;
      if (searchText.length === 0) return;
      searchRequest(searchText);
    };

    handleChange = (e) => {
      e.preventDefault();
      this.setState({
        searchText: e.target.value,
      }, () => {
        const { searchText } = this.state;
        this.searchThrottled(searchText);
      });
    };

    handleClick = (e) => {
      e.preventDefault();
      const { searchText } = this.state;
      this.searchThrottled(searchText);
    };

    handleClear = (e) => {
      e.preventDefault();
      this.setState(initialState);
    };

    render() {
      const { searchText } = this.state;
      const { projects } = this.props;
      return (
        <>
          <SearchComponent
            onChange={this.handleChange}
            onClick={this.handleClick}
            onClear={this.handleClear}
            searchText={searchText}
          />
          <ProjectListComponent projects={projects} />
        </>
      );
    }
}

App.propTypes = { projects: PropTypes.arrayOf(PropTypes.object).isRequired };

const mapStateToProps = state => ({
  projects: state.projects,
});

const mapDispatchToProps = dispatch => ({
  searchRequest: (searchQuery) => {
    dispatch(asyncSearch(searchQuery));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
